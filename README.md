# ThingsBoard实例

#### 介绍
学以致用
取之于开源
回馈于开源

* 本开源项目基于`ThingsBoard`社区版
   * 修改了仪表板可分层叠放
* 所有内容均是工作中的记录和生产实例导出文件
* 如果不明白项目或使用方法，首先需要了解一些TB的使用，多看源码


#### 文件夹说明

1.  无特殊说明均安文件夹名字理解内容
2.  `小部件/TB-settings` 对应 TB 项目中的 `ui-ngx/src/app/modules/home/components/widget/lib/settings` 文件夹

### 功能介绍

1. `小部件/Datav-Border` 可设置的border部件  
    <img src="images/1-border01.png" />
2. `仪表板/碳抵消数据管理.json` 给当前客户增加 `Attribute`  
    <img src="images/2-tzh.png" width = "400" />
3. [TB扩展/仪表板修改为层叠模式.md](TB扩展/仪表板修改为层叠模式.md)  
    <img src="images/3-df0.png" width = "400" />


### 感谢

1.  感谢所有网络中分享教程的老师们
2.  感谢[成都物讯网联科技有限公司 tb_energy_monitoring_public 开源项目](https://gitee.com/chengdu-iot-network/tb_energy_monitoring_public)，5星推荐