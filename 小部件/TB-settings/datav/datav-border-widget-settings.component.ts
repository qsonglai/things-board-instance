import { Component } from '@angular/core';
import { WidgetSettings, WidgetSettingsComponent } from '@shared/models/widget.models';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '@core/core.state';

@Component({
  selector: 'tb-datav-border-widget-settings',
  templateUrl: './datav-border-widget-settings.component.html',
  styleUrls: ['./../widget-settings.scss']
})
export class DatavBorderWidgetSettingsComponent extends WidgetSettingsComponent {

  datavBorderWidgetSettingsForm: UntypedFormGroup;

  constructor(protected store: Store<AppState>,
              private fb: UntypedFormBuilder) {
    super(store);
  }

  protected settingsForm(): UntypedFormGroup {
    return this.datavBorderWidgetSettingsForm;
  }

  protected defaultSettings(): WidgetSettings {
    return {
      color1: '#4FD2DD',
      color2: '#235FA7',
      dur: 0.5
    };
  }

  protected onSettingsSet(settings: WidgetSettings) {

    this.datavBorderWidgetSettingsForm = this.fb.group({

      // Common pie settings

      color1: [settings.color1, []],
      color2: [settings.color2, []],
      dur: [settings.dur, [Validators.min(0)]]
    });
  }
}
